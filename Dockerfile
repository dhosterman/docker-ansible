FROM python:3.10-slim

RUN apt-get update && \
    apt-get install -y --no-install-recommends curl ssh && \
    apt-get autoremove -y

RUN pip install --upgrade ansible

RUN ansible-galaxy collection install community.general community.docker
